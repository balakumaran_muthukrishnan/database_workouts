-- add column
use transport;
alter table vechicle add column drive_1 varchar(20) not null;
select * from vechicle;
describe vechicle;

--  change column
alter table vechicle change vechicle_manufacturing vechicle_manufacture varchar(100);
select * from vechicle;

-- modify
alter table vechicle modify model varchar(100) not null ,modify vechicle_type_id varchar(100) not null,modify manufacturing_country varchar(100);
describe vechicle;
alter table vechicle modify manufacturing_country varchar(100) not null;
describe vechicle;

-- drop
alter table vechicle drop drive;
describe vechicle;

-- create database
create database toothpaste;
use toothpaste;
create table product( id int primary key auto_increment not null, name varchar(200) not null);
alter table product change name pastename varchar(100);
select * from product;



-- DML

-- inserting
insert into product(pastename)values('colgate');
select * from product;
insert into product(pastename)values('dabur red'),('close up'),('himalaya'),('sensitive'),('colgate active salt');

-- change column 
alter table product change pastename tooth_paste_name varchar(100) not null;
select * from product;

-- updating
update product set pastename= 'neom' where id = '5';
select * from product;

-- delete
delete from product;
select * from product;

-- truncate
create database school;
use school;
create table student( id int primary key auto_increment not null, first_name varchar(200) not null, last_name varchar(100));
insert into student(first_name,last_name)values('ilan','kumaran'),('bala','kumaran'),('akash','kumar');
select * from student;
truncate student;

-- drop
drop database school;



