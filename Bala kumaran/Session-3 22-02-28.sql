use transport;
select * from vechicle;

-- create table vechicle_type

create table vechicle_type(
    id int primary key auto_increment not null,
    code varchar(200) not null,
    description varchar(200) not null
    );
    select * from vechicle_type;
    describe vechicle_type;
    
-- insert the value vechicle_type

insert into vechicle_type( code, description) values('auto','auto rickshaw'),
('truck','Quarry Lorry'),
('motorcycle','scooter'),
('car','Mahindra Bolero');

update vechicle_type set code = 'auto', description ='a vehicle in which all four wheels receive power from the engine to help with steering' where id = 1;
update vechicle_type set code = 'truck', description ='any motor vehicle designed to carry freight or goods or to perform special services ' where id = 2;
update vechicle_type set code = 'motorcycle', description ='any complete round or series of occurrences that repeats or is repeated ' where id = 3;
update vechicle_type set code = 'car', description ='a wheeled motor vehicle used for transportation ' where id = 4;

select * from vechicle_type;

-- create table product_details

create table product_details(
 id int primary key auto_increment not null,
 product_brand varchar(255),
 product_model varchar(255),
 price decimal(10,2)
 );

 -- alter table product_details
 
 ALTER TABLE product_details
ADD vechicle_type_id int , add FOREIGN KEY (vechicle_type_id) REFERENCES vechicle_type(id);
describe product_details;

 -- insert value product_details
insert into product_details( product_brand, product_model, price,vechicle_type_id) values
('audi','audi RR-34','750000',4),('truck','dodge','200000',2),('auto','pontiac','100000',1),('motorcycle','GSX-R-1000','100000',3);
update product_details set product_brand = 'suzuki', product_model ='scorpio WR-45', price ='650000',vechicle_type_id = 4 where id = 2;
update product_details set product_brand = 'audi', product_model ='audi RR-34', price ='750000',vechicle_type_id = 4 where id = 3;
update product_details set product_brand = 'truck', product_model ='dodge', price ='200000',vechicle_type_id = 2 where id = 4;
select * from product_details;


-- create table users

create table users(
    id int primary key auto_increment not null,
	name varchar(200) not null,
    contact_number varchar(200) not null,
    email varchar(100)
    );
  
    
-- insert the value users

insert into users( name, contact_number, email) values('ilan','7890654321','ilan@gmail.com'),('bala','6380124955','bala@gmail.com'),('valith','9600291149','valith@gmail.com');
update users set name = 'shiyam', contact_number ='6789054321', email = 'shiyam@gmail.com' where id = 2;
update users set name = 'akash', contact_number ='9087654321', email = 'akash@gmail.com' where id = 3;
select * from users;

-- create table purchase

create table purchase(
id int primary key auto_increment not null,
quantity int,
vechicle_price decimal(10,2),
user_id int,
vechicle_type_id int,
product_details_id int
);

-- insert the value purchase

insert into purchase( quantity, vechicle_price, user_id, vechicle_type_id, product_details_id) values(1,'200000',1,4,1),(1,'100000',2,3,2),(2,'150000',3,2,3);
select * from purchase;

-- alter the purchase table

ALTER TABLE purchase add FOREIGN KEY (product_details_id) REFERENCES product_details(id);
ALTER TABLE purchase add FOREIGN KEY (vechicle_type_id) REFERENCES vechicle_type(id);
ALTER TABLE purchase add FOREIGN KEY (user_id) REFERENCES users(id);

-- join the table
  select purchase.id, purchase.quantity, purchase.vechicle_price, 
  users.name, vechicle_type.code, product_details.product_brand, product_details.price *  purchase.quantity as product_total 
  from purchase,users,product_details,vechicle_type
  where purchase.user_id = users.id 
  and product_details.id = purchase.product_details_id 
  and vechicle_type.id = purchase.vechicle_type_id
  and users.id = purchase.user_id order by purchase.id;

