use transport;

select * from users;
insert into users(name,contact_number,email)values('mani',9870654321,'mani@gmail.com' ),
('mohan',9870777321,'mohan@gmail.com' ),('manoj',6670777321,'manoj@gmail.com' );
select * from product_details;

-- left join

select users.name,users.contact_number , product_details.product_model,product_details.price
from users
left  join purchase
on users.id = purchase.id
left join product_details
on purchase.id = product_details.id;


-- right join
select users.name,users.contact_number , product_details.product_model,product_details.price
from users
right  join purchase
on users.id = purchase.id
right join product_details
on purchase.id = product_details.id;




