use transport;

-- union
select users.name,users.contact_number , product_details.product_model,product_details.price
from users
left  join purchase
on users.id = purchase.id
left join product_details
on purchase.id = product_details.id
union 
select users.name,users.contact_number , product_details.product_model,product_details.price
from users
right  join purchase
on users.id = purchase.id
right join product_details
on purchase.id = product_details.id;

-- union all

 -- union
select users.name,users.contact_number , product_details.product_model,product_details.price
from users
left  join purchase
on users.id = purchase.id
left join product_details
on purchase.id = product_details.id
union all
select users.name,users.contact_number , product_details.product_model,product_details.price
from users
right  join purchase
on users.id = purchase.id
right join product_details
on purchase.id = product_details.id;



create table indian_company(
id int primary key auto_increment not null,
brand_name char(100),
owner_name varchar(50),
headquarters_address mediumtext not null);
insert into indian_company(brand_name,owner_name,headquarters_address)values
('tata','tata sons','Tata Motors Ltd. (Regional Office), Plot No. 37/38, ASV Ramana Towers, Venkatranarayana Road, T. Nagar, Chennai - 600017'),
('bajaj','rahul bajaj','No 804, 805, 806, Raheja Towers, 8th Floor, Delta Wing, Anna Salai, Mount Road, Chennai - 600002, Near LIC'),
('tvs','Sundaram','Sundaram Clayton Ltd. "Jayalakshmi Estates", 29 (Old No.8), Haddows Road
Chennai (Madras) - 600006
Tamil Nadu'),
('ashok leyland','hinduja group','Ashok Leyland Ltd.,
No.1, Sardar Patel Road,
Guindy, Chennai – 600 032'),
('mahindra',' anand mahindra','No 8, Depot Line, 137, Great Southern Trunk Rd, Lakshmi Nagar, Pallavaram, Chennai, Tamil Nadu 600043');
select * from indian_company;

-- cross join
select indian_company.brand_name,vechicle_type.code as vechicle_type
from indian_company
cross join vechicle_type;