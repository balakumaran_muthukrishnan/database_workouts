create database vechicle;
use vechicle;

-- table create

create table cars(
    id int primary key auto_increment not null,
    car_name varchar(100)not null,
    car_model varchar(100),
    description varchar(100),
    price decimal(10,2)
    );
    select * from cars;                            
    
-- insert table

insert into cars(car_name,car_model,description,price)values('honda','honda WR-V','Honda was the first Japanese automobile manufacturer to release a dedicated luxury',600000),
('audi','audi A-4','Audi AG is a world-renowned manufacturer of automobiles and motorcycles',800000),
('suzuki','Baleno','Suzuki manufactures automobiles, motorcycles, all-terrain vehicles (ATVs)',700000),
('rolls royce','ghost','Rolls-Royce is the world leading supplier of marine propulsion equipment.',1200000),
('bentley','flying spur','Bentley in 1919, and went from strength to strength  technology along with its breathtaking designs. ',600000),
('BMW','BMW X-5','BMW is a German automobile, motorcycle and engine manufacturing company founded in 1916',1000000);

-- create table

create table users(
    id int primary key auto_increment not null,
	name varchar(200) not null,
    contact_number varchar(200) not null,
    email varchar(100)
    );
    
-- insert table

 insert into users( name, contact_number, email) values
 ('ilan','7890654321','ilan@gmail.com'),
 ('bala','6380124955','bala@gmail.com'),
 ('valith','9600291149','valith@gmail.com'),
 ('akash','9600781129','akash@gmail.com'),
 ('shiyam','6789504321','shiyam@gmail.com'),
 ('manikandan','9780456321','mani@gmail.com');
 select * from users;
 
 -- create users
  
  create user bala_01@'localhost' identified by '';
  create user bala_02@'localhost' identified by '';
  create user bala_03@'localhost' identified by '';
  create user bala_04@'localhost' identified by '';
  create user bala_05@'localhost' identified by '';
  create user bala_06@'localhost' identified by '';
  
  -- global
  
  grant all on *.* to bala_01@'localhost';
  show grants for 'bala_01'@'localhost';
  
  -- database
  
  grant all on vechicle .* to bala_02@'localhost';
  show grants for 'bala_02'@'localhost';
  
  -- table
  
  grant all on vechicle.cars to bala_03@'localhost';
  show grants for 'bala_03'@'localhost';
  
  -- column
  grant select,update,update(car_name,car_model,description) on vechicle.cars to bala_04@'localhost';
  show grants for 'bala_04'@'localhost';
  
  -- proxy
  
  grant proxy on 'bala_04'@'localhost' to 'bala_05'@'localhost';
  show grants for 'bala_05'@'localhost';
  
  -- revoke
  
 grant proxy on 'bala_05'@'localhost' to 'bala_06'@'localhost';
 show grants for 'bala_06'@'localhost';
 revoke proxy on 'bala_05'@'localhost' from 'bala_06'@'localhost';
 show grants for 'bala_06'@'localhost';