-- stored procedures
use transport;

delimiter //
create procedure usp_TransportVechicle()
begin
select vechicle_type.code as vechicle_name,
product_details.product_brand,
users.name as customer_name,
purchase.quantity,purchase.vechicle_price,
purchase.quantity*purchase.vechicle_price as total
from purchase
inner join users
on purchase.user_id = users.id
inner join vechicle_type
on purchase.vechicle_type_id = vechicle_type.id
inner join product_details
on purchase.product_details_id = product_details.id
order by purchase.id;
end //
delimiter ;

 drop procedure usp_TransportVechicle;
 call usp_TransportVechicle();
 
 -- insert
 delimiter //
create procedure usp_InsertTransportVechicle()
begin
insert into indian_company(brand_name,owner_name,headquarters_address)values
('Hyundai Motors','Hyundai Motor Group','Hyundai Motor Ltd. (Regional Office), Plot No. 37/56, ASV  Towers, pillar Road, Ashok Nagar, Chennai - 600017');
end //
delimiter ;

 call usp_InsertTransportVechicle();
 select * from indian_company;
 
 --  update
 delimiter //
create procedure usp_UpdateTransportVechicle()
begin
update users set name='arun',email='arun@gmail.com'
where id=8;
end //
delimiter ;
 
drop procedure usp_UpdateTransportVechicle;
call usp_UpdateTransportVechicle;
select * from users;

-- delete

delimiter //
create procedure usp_DeleteTransportVechicle()
begin
delete from indian_company where id =6;
end //
delimiter ;

drop procedure usp_DeleteTransportVechicle;
call usp_DeleteTransportVechicle();
select * from indian_company;


-- in parameter
drop procedure if exists usp_UpdateCompanyNameByOwner;
delimiter //
create procedure usp_UpdateCompanyNameByOwner(in i_owner_name varchar(50))
begin
update indian_company set brand_name = 'marco bolo' where owner_name = i_owner_name;
end //
delimiter ;

call usp_UpdateCompanyNameByOwner(' anand mahindra');

-- out parameter
drop procedure if exists  usp_CountBrandName;
delimiter //
create procedure usp_CountBrandName(out count int)
begin
 select count(product_brand) from product_details into count ;
end //
delimiter ;
call usp_CountBrandName(@count) ;
select @count;