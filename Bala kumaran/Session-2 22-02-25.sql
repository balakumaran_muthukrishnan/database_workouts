use Transport;
select * from vechicle;


-- add column
alter table vechicle add column dealer varchar(50) not null, add column export_date varchar(30) not null ;
select * from vechicle;

-- insert values
insert into vechicle (dealer, export_date) values
('fiero','2022-02-11'),('fiero','2022-02-12'),
('fiero','2022-02-13'),('fiero','2022-02-14'),
('GT','2022-02-15'),('dakota','2022-02-16'),
('tohoe','2022-02-17'),('GSX','2022-02-18'),
('durango','2022-02-19'),('lotus','2022-02-20'),
('RS6','2022-02-21'),('A4','2022-02-22'),
('formula fort','2022-02-23'),('ford','2022-02-24'),
('scorpio','2022-02-25'),('A5','2022-02-26'),
('seven ford','2022-02-27'),('RSE','2022-02-28'),
('tahoe','2022-02-29'),('GSX','2022-02-30');
select * from vechicle;

-- update
update vechicle set dealer = 'fiero',export_date = '2021-11-03' where id = '1';
select * from vechicle;
update vechicle set dealer = 'fiero',export_date = '2021-11-05' where id = '2';
update vechicle set dealer = 'fiero',export_date = '2021-11-08' where id = '3';
update vechicle set dealer = 'fiero',export_date = '2021-11-11' where id = '4';
update vechicle set dealer = 'GT',export_date = '2021-11-15' where id = '5';
update vechicle set dealer = 'dakota',export_date = '2021-11-18' where id = '6';
update vechicle set dealer = 'tohoe',export_date = '2021-11-22' where id = '7';
update vechicle set dealer = 'GSX',export_date = '2021-12-01' where id = '8';
update vechicle set dealer = 'durango',export_date = '2021-12-06' where id = '9';
update vechicle set dealer = 'lotus',export_date = '2021-12-09' where id = '10';
update vechicle set dealer = 'RS6',export_date = '2021-12-12' where id = '11';
update vechicle set dealer = 'A4',export_date = '2021-12-16' where id = '12';
update vechicle set dealer = 'formula fort',export_date = '2021-12-19' where id = '13';
update vechicle set dealer = 'ford',export_date = '2021-12-28' where id = '14';
update vechicle set dealer = 'scorpio',export_date = '2022-01-02' where id = '15';
update vechicle set dealer = 'A5',export_date = '2022-01-07' where id = '16';
update vechicle set dealer = 'seven ford',export_date = '2022-01-10' where id = '17';
update vechicle set dealer = 'RSE',export_date = '2022-01-12' where id = '18';
update vechicle set dealer = 'tahoe',export_date = '2022-01-14' where id = '19';
update vechicle set dealer = 'GSX',export_date = '2022-01-17' where id = '20';
select * from vechicle;
delete from vechicle where id between 21 and 40;


-- last month record
select * from vechicle where month(export_date) = '12';

-- last 10 to 15 record
select * from vechicle order by id desc limit 10, 5;

 -- add column
 alter table vechicle add column price float(8) not null;
 select * from vechicle;
 alter table vechicle drop price;
 
 -- insert 
 insert into vechicle (price)values
 (100000.00),(100000.00),
 (100000.00),(100000.00),
 (150000.00),(250000.00),
 (300000.00),(100000.00),
 (500000.00),(600000.00),
 (100000.00),(200000.00),
 (400000.00),(550000.00),
 (650000.00),(250000.00),
 (300000.00),(200000.00),
 (450000.00),(100000.00);
  select * from vechicle;
update vechicle set price = 100000.00 where id = '1';
update vechicle set price = 100000.00 where id = '2';
update vechicle set price = 100000.00 where id = '3';
update vechicle set price = 100000.00 where id = '4';
update vechicle set price = 100000.00 where id = '5';
update vechicle set price = 200000.00 where id = '6';
update vechicle set price = 200000.00 where id = '7';
update vechicle set price = 500000.00 where id = '8';
update vechicle set price = 500000.00 where id = '9';
update vechicle set price = 100000.00 where id = '10';
update vechicle set price = 750000.00 where id = '11';
update vechicle set price = 650000.00 where id = '12';
update vechicle set price = 750000.00 where id = '13';
update vechicle set price = 200000.00 where id = '14';
update vechicle set price = 100000.00 where id = '15';
update vechicle set price = 250000.00 where id = '16';
update vechicle set price = 300000.00 where id = '17';
update vechicle set price = 200000.00 where id = '18';
update vechicle set price = 450000.00 where id = '19';
update vechicle set price = 100000.00 where id = '20';
select * from vechicle;
delete from vechicle  where id between 41 and 60;


-- Aggregate functions

-- count group by
select vechicle_manufacture,
count(vechicle_manufacture) from vechicle group by vechicle_manufacture;

-- sum group by
select vechicle_manufacture, sum(price) as total_price from vechicle group by vechicle_manufacture;
select sum(price) as total_price from vechicle;

-- group by having
select vechicle_manufacture,price from vechicle group by vechicle_manufacture having price > 500000;

-- order by ascending
select vechicle_type_id from vechicle order by vechicle_type_id asc;

-- order by descending
select price from vechicle order by price desc;

-- where
select  manufacturing_country, model from vechicle where manufacturing_country = 'united state';