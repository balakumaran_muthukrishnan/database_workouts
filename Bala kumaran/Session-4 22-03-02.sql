use transport;

-- create subquery
select model, vechicle_type_id, dealer, price from vechicle where id in(select id from vechicle where price > 300000);


-- subquery row
select *  from product_details where(vechicle_type_id, price) = 
(select vechicle_type_id, price from product_details where vechicle_type_id = 1);
 
-- sub query and all
select * from vechicle where vechicle_type_id = all (select id from vechicle_type where code ='car');


-- subquery and any
select product_brand, product_model from product_details where id = any (select id from users where name ='ilan');

-- subquery and exists
select product_brand, product_model from product_details where exists (select id from users where email ='ilan@gmail.com');

-- subquery and not exists
select product_brand, product_model from product_details where !exists (select id from users where email ='i@gmail.com');

-- subquery convert in to join
select vechicle_type.code as vechicle_name,
product_details.product_brand,
users.name as customer_name,
purchase.quantity,purchase.vechicle_price,
purchase.quantity*purchase.vechicle_price as total
from purchase
inner join users
on purchase.user_id = users.id
inner join vechicle_type
on purchase.vechicle_type_id = vechicle_type.id
inner join product_details
on purchase.product_details_id = product_details.id
order by purchase.id;




