use transport;

create table data_type_reference( id int primary key auto_increment not null,
model_name char(200),
model_year year not null,
vechicle_type_id smallint not null,
vechicle_manufacture tinytext not null,
engine_size float(8),
engine_type varchar(30),
tranmission varchar(100),
manufacturing_country mediumtext not null,
dealer text not null,
export_date date not null,
price decimal(9,2) not null);
drop table data_type_reference;
desc data_type_reference;

insert into data_type_reference(model_name,model_year,
vechicle_type_id,vechicle_manufacture,engine_size, 
engine_type, tranmission,manufacturing_country,dealer,
export_date,price)values
('fiero 2M4',1985,1,'pontiac',2.5,'I-4','5-speed manual','United state','fiero','2021-01-11',100000.00),
('scorpio 2M4',2010,2,'scorpio WR-45',3.5,'I-5','6-speed manual','india','scorpio','2021-07-23',400000.00),
('BMW R-5',2011,3,'BMW XR-7',4.5,'I-6','7-speed manual','united state','BMW','2021-05-23',600000.00);
select * from data_type_reference;