-- create database
drop database amazon;
create database amazon;
use amazon;
drop table orders;

-- primary key , foreign key , unique key and not null
create table customer(id int primary key auto_increment not null, first_name varchar(100), last_name varchar(100), age int(100), phone_no bigint not null, unique(phone_no));
create table orders(orders_id int primary key auto_increment not null, order_number varchar(200), phone_no bigint, foreign key(phone_no) references customer(phone_no));
describe customer;
describe orders;

-- insert values customer
insert into customer (first_name,last_name,age,phone_no) values ('Bala','kumaran','23','6380124955'),
('ilan','kumaran','23','9876453201'),('akash','kumaran','23','6785423190'),('shiyam','vignesh','23','9870654321');
select * from customer;
describe customer;

-- insert value orders
insert into orders (order_number,phone_no) values ('23456',6380124955),
('56784',9876453201),('78906',6785423190),('78690',9870654321);
select * from orders;
describe orders;
truncate orders;

-- default

use amazon;
alter table customer add column city varchar(50) not null default 'madurai';
select * from customer;

-- check table

alter table customer add check (age >= 18);
describe customer;
insert into customer (first_name,last_name,age,phone_no) values ('valith','syed','23','9880124955');
select * from customer;