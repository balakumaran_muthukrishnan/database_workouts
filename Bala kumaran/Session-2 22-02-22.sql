create database Transport;
use Transport;
create table vechicle(
    id int primary key auto_increment not null,
    model varchar(200) not null,
    model_year varchar(100),
    vechicle_type_id varchar(100),
    vechicle_manufacturing varchar(200),
    engine_size varchar(100),
    engine_type varchar(100),
    transmission varchar(200),
    manufacturing_country varchar(100)
    );
    select * from vechicle;
    insert into vechicle (model,model_year,vechicle_type_id,vechicle_manufacturing,engine_size, engine_type, transmission,manufacturing_country)
    values ('fiero 2M4','1985','Auto','pontiac','2.5','I-4','5-speed manual','United state');
    select * from vechicle;
    insert into vechicle (model,model_year,vechicle_type_id,vechicle_manufacturing,engine_size, engine_type, transmission,manufacturing_country)
    values ('fiero 2M4','1985','Auto','pontiac','2.5','I-4','5-speed manual','United state'),
           ('fiero 2M5','1986','Auto','pontiac','2.5','I-4','6-speed manual','United state'),
           ('fiero 2M6','1987','Auto','pontiac','2.5','I-4','7-speed manual','United state'),
           ('3000 GT VR-4','1991','Auto','Mitsubsi','3.0','V-6','5-speed manual','Japan'),
           ('Dakota','2004','Truck','dodge','4.2','V-8','5-speed manual','united state'),
           ('Tahoe','1996','SUV','chevrolet','5.7','V-8','4-speed automatic','germany'),
           ('GSX-R-1000','2006','Motorcycle','suzuki','1.0','I-4','6-speed manual','france'),
           ('durango','2007','car','dodge','5.7','V-8','5-speed automatic','italy'),
           ('lotus','2008','minibus','elise','4.2','I-4','6-speed manual','united kingdom'),
           ('RS6','2010','motorbike','Audi','4.7','V-8','7-speed automatic','china'),
           ('A4','2011','minivan','citation','2.7','I-4','6-speed manual','united kingdom'),
           ('formula ford','2012','Truck','BMW','4.5','Y-3','5-speed automatic','singapore'),
           ('ford','2013','car','Audi','5.6','T-8','7-speed manual','India'),
           ('scorpio','2015','car','scorpio WR-45','6.8','B-6','7-speed automatic','India');
select * from vechicle;
insert into vechicle(model,model_year,vechicle_type_id,vechicle_manufacturing,engine_size, engine_type, transmission,manufacturing_country)
values  ('A5','2010','minivan','citation','2.8','I-4','6-speed manual','united state'),
		('seven ford','2011','bus','BMW','4.9','Y-8','5-speed automatic','oman'),
		('RSE-24','2016','car','Audi','8.4','S-8','7-speed manual','united kingdom'),
		('Tahoe-56','2016','SUV-6','chevrolet','5.7','V-8','4-speed automatic','germany'),
		('GSX-R-2000','2017','Motorcycle','suzuki','1.0','I-4','6-speed manual','france');
select * from vechicle;
drop table vechicle;